const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth")

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if (data.isAdmin){

		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(false)
	}

});

// retrieve all courses
router.get ("/allCourses", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send (resultFromController));
})


// retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send (resultFromController));
});


// retrieve all details 
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send (resultFromController));
});

/*
// updating a course 
// JWT verification is needed to ensure that user is logged in before updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})
*/

// (ASSIGNMENT)
// updating a course 
router.put("/:courseId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});

// archive a course

router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/enroll", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);

	let course = {
		userId : req.body.userId,
	}
	userController.enroll(course, user).then(resultFromController => res.send(resultFromController));
});



module.exports = router;
