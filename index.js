const express = require("express");
const mongoose = require("mongoose");

// it allows our backend app to be available to our frontend app
// cross-origin resource
const cors = require("cors");

//allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes")

const app = express();

// mongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.z2a08wk.mongodb.net/b244_booking?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	});

mongoose.connection.once('open', () => 
	console.log('Now connected to the MongoDB Atlas.'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// defines "/users" to be included to all the iser routes defined in the uesrRoutes file
app.use("/users", userRoutes);

app.use("/courses", courseRoutes);

// will use the difined port number for the application whenever an environment variable is available or will ise the port 4000 if none is defined
//this syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online at port ${process.env.PORT || 4000}`)
});