const bcrypt = require("bcrypt");
const auth = require("../auth")
const User = require("../models/User");
const Course = require("../models/Course");

// check if the email already exists
/*
	steps:
	1. Use "mongoose" "find method" to find duplicate emails
	2. .then method to send a response back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the find method returns a record if a match is found
		if ( result.length > 0) {
			return true;

		// no duplicate email found
		// the user is not yet registered in the database	
		} else {
			return false;
		}
	})
};


// 
// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) =>  {

	// create a variable "newUser" and instantiates a new "User" object using the mongoose model
	// uses the information from the request body to provide all the necessary information
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,

		// syntax: bcrpyt.hashSync(dataToBeEncypted, salt)

		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// saves the created object to our database
	return newUser.save().then((user, error) => {

		// user registration failed
		if (error) {
			return false;

		// User registration is successful
		} else {
			return true;
		}
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {

			// creates variable to return the result of comparing the login form password and the database password
			// "compareSync" is used to compare a non encrypted password from the login form to the encrypted password from the database and returns "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// if the password matches 
			if(isPasswordCorrect) {

				// Generate an access token
				// it uses the "createAccessToken" method that we defined inm the auth.js file
				// returning an object back to the frontend application is common practice to ensure information is properly labeled.
				return {acess: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}


// activity s38
/*

module.exports.getProfile =(reqBody) => {
	return User.findById({_id: reqBody.id}).then(user => {
		if (user == null){
			return false
		} else {
			user.password = ""
				return user;
	})
}

*/

// solution 
module.exports.getProfile =(reqBody) => {
	return User.findById(reqBody.userId).then(result => {

		
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the info
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application

		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
	
};		

// enroll a user to a course
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/

/*module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({userId : data.userId});

		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId})

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	});

	let isEnrolled = courseToEnroll.enrollees.find(
    (user) => user.userId == data.userId);

	if (isUserUpdated && isCourseUpdated && !isEnrolled) {
		return true;
	} else {
		return false;
	}
}
*/
// activity

module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId);
  let isCourseUpdated = await Course.findById(data.courseId);
  let isEnrolled = isCourseUpdated.enrollees.find(
    (user) => user.userId == data.userId );
  
  if (isUserUpdated && isCourseUpdated && !isEnrolled) {
		isUserUpdated.enrollments.push({ courseId: data.courseId });
		isUserUpdated.save();
		isCourseUpdated.enrollees.push({ userId: data.userId });
		isCourseUpdated.save();
    return true;
  } else {
    return false;
  }
};

// try catch
/*
module.exports.enroll = async (data) => {
    try {
        const { userId, courseId } = data;

        const user = await User.findById(userId);
        user.enrollments.push({ courseId });
        await user.save();
        
        const course = await Course.findById(courseId);
        course.enrollees.push({ userId });
        await course.save();
        
        return true;
    } catch (error) {
        return false;
    }
}

*/